let $ = document

const trex = $.querySelector('.trex')
const cactos = $.querySelector('.cactos')
const cloud1 = $.querySelector('.cloud1')
const cloud2 = $.querySelector('.cloud2')
const cloud3 = $.querySelector('.cloud3')
const gameover = $.querySelector('.gameover')
const restart = $.querySelector('.restart')
const resultScore = $.querySelector('.result')
const audioJump = $.querySelector('.jump-sound')
const audioGameOver = $.querySelector('.game-over-sound')

let counter = 0

function jump() {
    if (trex.classList != 'jump') {
        trex.classList.add('jump')
    }
    setTimeout(function () {
        trex.classList.remove('jump')
    }, 400)
}

function scoreUpdate() {
    counter++
    resultScore.innerHTML = counter
    if (counter > 15) {
        cactos.style.animation = 'block 1s infinite linear'
        cloud1.style.animation = 'block 1s infinite linear'
        cloud2.style.animation = 'block 2s infinite linear'
        cloud3.style.animation = 'block 3s infinite linear'
    }
}

let trexDead = setInterval(function () {

    let trexTop = parseInt(getComputedStyle(trex).getPropertyValue('top'))

    let cactosLeft = parseInt(getComputedStyle(cactos).getPropertyValue('left'))

    if (cactosLeft < 50 && cactosLeft > 0 && trexTop >= 180) {
        audioGameOver.play()
        gameover.style.display = 'flex'
        cactos.classList.add('stop')
        trex.classList.add('stop')
    }

}, 10)

restart.addEventListener('click', function () {
    location.reload()
    counter = 0
    scoreUpdate()
})
window.addEventListener('keydown', function (event) {
    if (event.key == 'Enter') {
        location.reload()
        counter = 0
        scoreUpdate()
    }
})

window.addEventListener('click', function () {
    jump()
})
window.addEventListener('keypress', function (event) {
    if (event.code == 'Space') {
        jump()
    }
})

cactos.addEventListener('animationiteration', scoreUpdate)